<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Translation Example</source>
        <translation>Übersetzungsbeispiel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>Click me!</source>
        <translation>Drück mich!</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="45"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>Deutsch</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="90"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="92"/>
        <source>Hello, World!</source>
        <translation>Hallo, Welt!</translation>
    </message>
</context>
</TS>

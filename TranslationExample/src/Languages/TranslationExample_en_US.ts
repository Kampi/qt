<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Translation Example</source>
        <translation>Translation Example</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>Click me!</source>
        <translation>Click me!</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="45"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="62"/>
        <source>Deutsch</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="90"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="92"/>
        <source>Hello, World!</source>
        <translation>Hello, World!</translation>
    </message>
</context>
</TS>

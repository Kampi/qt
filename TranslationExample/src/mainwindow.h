#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QMainWindow>
#include <QMessageBox>
#include <QTranslator>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    protected:
        void changeEvent(QEvent* event);

    protected slots:
        void slotLanguageChanged(bool checked);

    public:
        MainWindow(QWidget* parent = nullptr);
        ~MainWindow();

    private slots:
        void on_pushButton_Message_clicked();

    private:
        Ui::MainWindow* _mUi;
        QTranslator _mTranslator;

        void _switchLanguage(QString Language);
};
#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), _mUi(new Ui::MainWindow)
{
    _mUi->setupUi(this);

    QActionGroup* actionGroup = new QActionGroup(this);
    actionGroup->addAction(_mUi->action_German);
    actionGroup->addAction(_mUi->action_English);
    actionGroup->setExclusive(true);

    connect(_mUi->action_German, &QAction::triggered, this, &MainWindow::slotLanguageChanged);
    connect(_mUi->action_English, &QAction::triggered, this, &MainWindow::slotLanguageChanged);

    // Use german as default language
    _mUi->action_German->triggered(true);
}

MainWindow::~MainWindow()
{
    delete _mUi;
}

void MainWindow::changeEvent(QEvent* event)
{
    switch(event->type())
    {
        case QEvent::LanguageChange:
        {
            _mUi->retranslateUi(this);
            break;
        }
        case QEvent::LocaleChange:
        {
            QString Locale = QLocale::system().name();
            Locale.truncate(Locale.lastIndexOf('_'));
            this->_switchLanguage(Locale);

            break;
        }
        default:
        {
            break;
        }
    }

    QMainWindow::changeEvent(event);
}

void MainWindow::slotLanguageChanged(bool)
{
    QAction* Sender = qobject_cast<QAction*>(QObject::sender());
    Sender->setChecked(true);
    this->_switchLanguage(Sender->text());
}

void MainWindow::_switchLanguage(QString Language)
{
    QLocale::setDefault(QLocale(Language));

    qApp->removeTranslator(&_mTranslator);

    if(_mTranslator.load(QString(QApplication::applicationDirPath().append(QString("/Languages/TranslationExample_%1.qm").arg(Language)))))
    {
        qApp->installTranslator(&_mTranslator);
        _mUi->statusbar->showMessage(QString("Language: %1").arg(Language));
    }
}

void MainWindow::on_pushButton_Message_clicked()
{
    QMessageBox* MsgBox = new QMessageBox;
    MsgBox->setWindowTitle(tr("Message"));
    MsgBox->setIcon(QMessageBox::Information);
    MsgBox->setText(tr("Hello, World!"));
    MsgBox->show();
}

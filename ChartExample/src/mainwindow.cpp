#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent),
                                          _mUi(new Ui::MainWindow),
                                          _mChart(new QChart()),
                                          _mSeries(new QSplineSeries(this)),
                                          _mAxisX(new QDateTimeAxis()),
                                          _mAxisY(new QValueAxis()),
                                          _mTimer(new QTimer(this)),
                                          _mColor(QColor(Qt::red)),
                                          _mPrevious(QDateTime::currentDateTime().toSecsSinceEpoch()),
                                          _mTime(60)
{
    _mUi->setupUi(this);

    _mPen = QPen(_mColor, 3);

    _mChart->setAnimationOptions(QChart::AllAnimations);
    _mChart->addSeries(_mSeries);
    _mChart->addAxis(_mAxisY, Qt::AlignLeft);
    _mChart->addAxis(_mAxisX, Qt::AlignBottom);
    _mChart->setAcceptHoverEvents(true);

    _mAxisY->setRange(-2, 2);

    _mAxisX->setTickCount(10);
    _mAxisX->setFormat("hh:mm:ss");
    _mAxisX->setTitleText("Date");
    _mAxisX->setMin(QDateTime::currentDateTime().addSecs(-_mTime / 4 * 3));
    _mAxisX->setMax(QDateTime::currentDateTime().addSecs(_mTime / 4));

    _mSeries->setPen(_mPen);
    _mSeries->setName("Data");
    _mSeries->attachAxis(_mAxisX);
    _mSeries->attachAxis(_mAxisY);

    connect(_mSeries, &QLineSeries::hovered, this, &MainWindow::lineToolTip);

    connect(_mTimer, &QTimer::timeout, this, &MainWindow::handleTimeout);
    _mTimer->start(1000);

    _mView = new QChartView(_mChart);
    _mView->setRubberBand(QChartView::HorizontalRubberBand);

    this->setCentralWidget(_mView);
}

MainWindow::~MainWindow()
{
    delete _mUi;
}

void MainWindow::on_action_Color_triggered()
{
    _mColor = QColorDialog::getColor(_mColor, this, "Choose color");
    if(_mColor.isValid())
    {
        _mSeries->setColor(_mColor);
    }
}

void MainWindow::on_action_Export_triggered()
{
    QString FileName = QFileDialog::getSaveFileName(this, "Export chart", "", "Portable Network Graphic (*.png)");

    if(FileName.length())
    {
        QPixmap Pixmap = _mView->grab();
        Pixmap.save(FileName);
    }
}

void MainWindow::handleTimeout()
{
    QDateTime Now = QDateTime::currentDateTime();

    qreal dx = _mChart->plotArea().width() / _mTime;
    _mSeries->append(Now.toMSecsSinceEpoch(), QRandomGenerator::global()->bounded(2));
    _mChart->scroll(dx * (Now.toSecsSinceEpoch() - _mPrevious), 0);

    if(_mSeries->count() > 300)
    {
        _mSeries->removePoints(0, _mSeries->count() - 300);
    }

    _mPrevious = Now.toSecsSinceEpoch();
}

void MainWindow::lineToolTip(QPointF point, bool state)
{
    if(state)
    {
        qDebug() << QDateTime::fromMSecsSinceEpoch(point.x());
    }
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key::Key_Escape)
    {
        this->close();
    }
}

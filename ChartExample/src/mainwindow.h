#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QTimer>
#include <QtCharts>
#include <QRandomGenerator>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    protected:
        void keyPressEvent(QKeyEvent* event);

    public:
        MainWindow(QWidget* parent = nullptr);
        ~MainWindow();

    private slots:
        void on_action_Color_triggered();
        void on_action_Export_triggered();
        void handleTimeout();
        void lineToolTip(QPointF point, bool state);

    private:
        Ui::MainWindow* _mUi;
        QChartView* _mView;
        QChart* _mChart;
        QSplineSeries* _mSeries;
        QDateTimeAxis* _mAxisX;
        QValueAxis* _mAxisY;
        QTimer* _mTimer;

        QPen _mPen;
        QColor _mColor;

        qint64 _mPrevious;

        double _mTime;
};
#endif // MAINWINDOW_H

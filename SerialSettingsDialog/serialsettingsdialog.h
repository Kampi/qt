#ifndef SERIALSETTINGSDIALOG_H
#define SERIALSETTINGSDIALOG_H

#include <QDebug>
#include <QDialog>
#include <QPushButton>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QAbstractButton>

namespace Ui
{
    class SerialSettingsDialog;
}

class SerialSettingsDialog : public QDialog
{
    Q_OBJECT

    public:
        struct Settings_t
        {
            QString Name;
            qint32 Baudrate;
            QSerialPort::DataBits Databits;
            QSerialPort::Parity Parity;
            QSerialPort::StopBits StopBits;
            QSerialPort::FlowControl FlowControl;
        };

    public:
        explicit SerialSettingsDialog(Settings_t Settings, QWidget* parent = nullptr);
        ~SerialSettingsDialog();

        Settings_t currentSettings() const;

    private slots:
        void on_buttonBox_Close_clicked(QAbstractButton* button);
        void on_comboBox_Ports_currentIndexChanged(const int index);
        void on_comboBox_Baudrate_currentIndexChanged(int index);
        void on_comboBox_Databits_currentIndexChanged(int index);
        void on_comboBox_Parity_currentIndexChanged(int index);
        void on_comboBox_Stopbits_currentIndexChanged(int index);
        void on_comboBox_FlowControl_currentIndexChanged(int index);

    private:
        void _fillBaudrate(void);
        void _fillDatabits(void);
        void _fillParity(void);
        void _fillStopBits(void);
        void _fillFlowControl(void);
        void _fillPorts(void);
        void _updateGUI(Settings_t Settings);

        Ui::SerialSettingsDialog* _mUi;

        Settings_t _mCurrentSettings;
};

#endif // SERIALSETTINGSDIALOG_H

#include "serialsettingsdialog.h"
#include "ui_serialsettingsdialog.h"

SerialSettingsDialog::SerialSettingsDialog(Settings_t Settings, QWidget* parent) : QDialog(parent),
                                                                                   _mUi(new Ui::SerialSettingsDialog)
{
    _mUi->setupUi(this);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowFlags(this->windowFlags() | Qt::MSWindowsFixedSizeDialogHint);

    this->_fillPorts();
    this->_fillBaudrate();
    this->_fillDatabits();
    this->_fillParity();
    this->_fillStopBits();
    this->_fillFlowControl();
    this->_updateGUI(Settings);

    _mUi->buttonBox_Close->button(QDialogButtonBox::Ok)->setText(tr("Ok"));
    _mUi->buttonBox_Close->button(QDialogButtonBox::Cancel)->setText(tr("Abbrechen"));
}

SerialSettingsDialog::~SerialSettingsDialog()
{
    delete _mUi;
}

SerialSettingsDialog::Settings_t SerialSettingsDialog::currentSettings() const
{
    return _mCurrentSettings;
}

void SerialSettingsDialog::_fillBaudrate(void)
{
    _mUi->comboBox_Baudrate->addItem("1200", QSerialPort::Baud1200);
    _mUi->comboBox_Baudrate->addItem("2400", QSerialPort::Baud2400);
    _mUi->comboBox_Baudrate->addItem("4800", QSerialPort::Baud4800);
    _mUi->comboBox_Baudrate->addItem("9600", QSerialPort::Baud9600);
    _mUi->comboBox_Baudrate->addItem("19200", QSerialPort::Baud19200);
    _mUi->comboBox_Baudrate->addItem("38400", QSerialPort::Baud38400);
    _mUi->comboBox_Baudrate->addItem("57600", QSerialPort::Baud57600);
    _mUi->comboBox_Baudrate->addItem("115200", QSerialPort::Baud115200);
    _mUi->comboBox_Baudrate->addItem(tr("Unknown"), QSerialPort::UnknownBaud);
}

void SerialSettingsDialog::_fillDatabits(void)
{
    _mUi->comboBox_Databits->addItem("5 " + tr("data bits"), QSerialPort::Data5);
    _mUi->comboBox_Databits->addItem("6 " + tr("data bits"), QSerialPort::Data6);
    _mUi->comboBox_Databits->addItem("7 " + tr("data bits"), QSerialPort::Data7);
    _mUi->comboBox_Databits->addItem("8 " + tr("data bits"), QSerialPort::Data8);
    _mUi->comboBox_Databits->addItem(tr("Unknown"), QSerialPort::UnknownDataBits);
}

void SerialSettingsDialog::_fillParity(void)
{
    _mUi->comboBox_Parity->addItem(tr("No parity"), QSerialPort::NoParity);
    _mUi->comboBox_Parity->addItem(tr("Even"), QSerialPort::EvenParity);
    _mUi->comboBox_Parity->addItem(tr("Odd"), QSerialPort::OddParity);
    _mUi->comboBox_Parity->addItem(tr("Space"), QSerialPort::SpaceParity);
    _mUi->comboBox_Parity->addItem(tr("Mark"), QSerialPort::MarkParity);
    _mUi->comboBox_Parity->addItem(tr("Unknown"), QSerialPort::UnknownParity);
}

void SerialSettingsDialog::_fillStopBits(void)
{
    _mUi->comboBox_Stopbits->addItem("1 " + tr("Stopbit"), QSerialPort::OneStop);
    _mUi->comboBox_Stopbits->addItem("1 1/2 " + tr("Stopbit"), QSerialPort::OneAndHalfStop);
    _mUi->comboBox_Stopbits->addItem("2 " + tr("Stopbit"), QSerialPort::TwoStop);
    _mUi->comboBox_Stopbits->addItem(tr("Unknown"), QSerialPort::UnknownStopBits);
}

void SerialSettingsDialog::_fillFlowControl(void)
{
    _mUi->comboBox_FlowControl->addItem(tr("Disabled"), QSerialPort::NoFlowControl);
    _mUi->comboBox_FlowControl->addItem(tr("Hardware"), QSerialPort::HardwareControl);
    _mUi->comboBox_FlowControl->addItem(tr("Software"), QSerialPort::SoftwareControl);
    _mUi->comboBox_FlowControl->addItem(tr("Unknown"), QSerialPort::UnknownFlowControl);
}

void SerialSettingsDialog::_fillPorts(void)
{
    _mUi->comboBox_Ports->clear();
    QString Description;
    QString Manufacturer;
    QString SerialNumber;
    for(const QSerialPortInfo& Info : QSerialPortInfo::availablePorts())
    {
        QStringList List;
        Description = Info.description();
        Manufacturer = Info.manufacturer();
        SerialNumber = Info.serialNumber();
        List << Info.portName()
             << (!Description.isEmpty() ? Description : "")
             << (!Manufacturer.isEmpty() ? Manufacturer : "")
             << (!SerialNumber.isEmpty() ? SerialNumber : "")
             << Info.systemLocation()
             << (Info.vendorIdentifier() ? QString::number(Info.vendorIdentifier(), 16) : "")
             << (Info.productIdentifier() ? QString::number(Info.productIdentifier(), 16) : "");

        _mUi->comboBox_Ports->addItem(List.at(0) + " - " + List.at(1), List.at(0));
    }
}

void SerialSettingsDialog::_updateGUI(SerialSettingsDialog::Settings_t Settings)
{
    int Index;

    Index = _mUi->comboBox_Ports->findText(Settings.Name, Qt::MatchContains);
    if(Index != -1)
    {
        _mUi->comboBox_Ports->setCurrentIndex(Index);
    }

    Index = _mUi->comboBox_Baudrate->findData(Settings.Baudrate);
    if(Index != -1)
    {
        _mUi->comboBox_Baudrate->setCurrentIndex(Index);
    }

    Index = _mUi->comboBox_Databits->findData(Settings.Databits);
    if(Index != -1)
    {
        _mUi->comboBox_Databits->setCurrentIndex(Index);
    }

    Index = _mUi->comboBox_Parity->findData(Settings.Parity);
    if(Index != -1)
    {
        _mUi->comboBox_Parity->setCurrentIndex(Index);
    }

    Index = _mUi->comboBox_Stopbits->findData(Settings.StopBits);
    if(Index != -1)
    {
        _mUi->comboBox_Stopbits->setCurrentIndex(Index);
    }

    Index = _mUi->comboBox_FlowControl->findData(Settings.FlowControl);
    if(Index != -1)
    {
        _mUi->comboBox_FlowControl->setCurrentIndex(Index);
    }
}

void SerialSettingsDialog::on_buttonBox_Close_clicked(QAbstractButton* button)
{
    if(button == _mUi->buttonBox_Close->button(QDialogButtonBox::Save))
    {
        this->accept();
    }
    else if(button == _mUi->buttonBox_Close->button(QDialogButtonBox::Discard))
    {
        this->reject();
    }
}

void SerialSettingsDialog::on_comboBox_Ports_currentIndexChanged(int index)
{
    _mCurrentSettings.Name = _mUi->comboBox_Ports->itemData(index).toString();
}

void SerialSettingsDialog::on_comboBox_Baudrate_currentIndexChanged(int)
{
    _mCurrentSettings.Baudrate = static_cast<QSerialPort::DataBits>(_mUi->comboBox_Baudrate->itemData(_mUi->comboBox_Baudrate->currentIndex()).toInt());
}

void SerialSettingsDialog::on_comboBox_Databits_currentIndexChanged(int)
{
    _mCurrentSettings.Databits = static_cast<QSerialPort::DataBits>(_mUi->comboBox_Databits->itemData(_mUi->comboBox_Databits->currentIndex()).toInt());
}

void SerialSettingsDialog::on_comboBox_Parity_currentIndexChanged(int)
{
    _mCurrentSettings.Parity = static_cast<QSerialPort::Parity>(_mUi->comboBox_Parity->itemData(_mUi->comboBox_Parity->currentIndex()).toInt());
}

void SerialSettingsDialog::on_comboBox_Stopbits_currentIndexChanged(int)
{
    _mCurrentSettings.StopBits = static_cast<QSerialPort::StopBits>(_mUi->comboBox_Stopbits->itemData(_mUi->comboBox_Stopbits->currentIndex()).toInt());
}

void SerialSettingsDialog::on_comboBox_FlowControl_currentIndexChanged(int)
{
    _mCurrentSettings.FlowControl = static_cast<QSerialPort::FlowControl>(_mUi->comboBox_FlowControl->itemData(_mUi->comboBox_FlowControl->currentIndex()).toInt());
}

#include "tabledata.h"

TableData::TableData()
{
    _mX = 0;
    _mY = 0;
}

TableData::TableData(double x, double y)
{
    _mX = x;
    _mY = y;
}

double TableData::X() const
{
    return _mX;
}

void TableData::setX(double X)
{
    _mX = X;
}

double TableData::Y() const
{
    return _mY;
}

void TableData::setY(double Y)
{
    _mY = Y;
}

#ifndef TABLEDATA_H
#define TABLEDATA_H

class TableData
{
    public:
        TableData();
        TableData(double x, double y);

        double X() const;
        void setX(double x);

        double Y() const;
        void setY(double y);

    private:
        double _mX;
        double _mY;
};

#endif // TABLEDATA_H

#include "tablemodel.h"

TableModel::TableModel(QVector<TableData>& Data, QObject* parent) : QAbstractTableModel(parent),
                                                                    _mData(Data)
{
}

int TableModel::rowCount(const QModelIndex&) const
{
    return _mData.size();
}

int TableModel::columnCount(const QModelIndex&) const
{
    return 2;
}

QVariant TableModel::data(const QModelIndex& index, int role) const
{
    if((role == Qt::DisplayRole) || (role == Qt::EditRole))
    {
        switch(index.column())
        {
            case 0:
            {
                return QString("%1").arg(_mData.at(index.row()).X());
            }
            case 1:
            {
                return QString("%1").arg(_mData.at(index.row()).Y());
            }
            default:
            {
                return QVariant();
            }
        }
    }

    return QVariant();
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 0:
            {
                _mData[index.row()].setX(value.toDouble());

                return true;
            }
            case 1:
            {
                _mData[index.row()].setY(value.toDouble());

                return true;
            }
            default:
            {
                return false;
            }
        }
    }

    emit QAbstractItemModel::dataChanged(index, index);

    return false;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal)
    {
        if(role == Qt::DisplayRole)
        {
            switch(section)
            {
                case 0:
                {
                    return "x";
                }
                case 1:
                {
                    return "y";
                }
                default:
                {
                    return QVariant();
                }
            }
        }
    }
    else if(orientation == Qt::Vertical)
    {
        if(role == Qt::DisplayRole)
        {
            return section + 1;
        }
    }

    return QVariant();
}

Qt::ItemFlags TableModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags Flags = Qt::ItemIsEditable | QAbstractTableModel::flags(index);

    return Flags;
}

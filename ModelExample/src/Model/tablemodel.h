#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QDebug>
#include <QString>
#include <QAbstractTableModel>

#include "tabledata.h"

class TableModel : public QAbstractTableModel
{
    Q_OBJECT

    public:
        explicit TableModel(QVector<TableData>& Data, QObject* parent = nullptr);

        int rowCount(const QModelIndex& Parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& Parent = QModelIndex()) const override;

        QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
        bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

        Qt::ItemFlags flags(const QModelIndex& index) const override;

    private:
        QVector<TableData> _mData;
};

#endif // TABLEMODEL_H

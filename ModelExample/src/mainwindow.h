#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>
#include <QVector>
#include <QMainWindow>
#include <QRandomGenerator>

#include "tabledata.h"
#include "tablemodel.h"
#include "spinboxdelegate.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget* Parent = nullptr);
        ~MainWindow();

    private:
        Ui::MainWindow* _mUi;

        QVector<TableData> _mData;
        SpinBoxDelegate _mDelegate;
        TableModel* _mModel;
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* Parent) : QMainWindow(Parent),
                                          _mUi(new Ui::MainWindow)
{
    QRandomGenerator RNG;

    _mUi->setupUi(this);

    for(int i = 0; i < 6; i++)
    {
        _mData.push_back(TableData(i, RNG.generateDouble()));
    }

    _mModel = new TableModel(_mData);

    _mUi->tableView->setModel(_mModel);
    _mUi->tableView->setItemDelegate(&_mDelegate);
}

MainWindow::~MainWindow()
{
    delete _mModel;
    delete _mUi;
}

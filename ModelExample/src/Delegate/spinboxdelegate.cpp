#include "spinboxdelegate.h"

SpinBoxDelegate::SpinBoxDelegate(QObject* parent) : QStyledItemDelegate(parent)
{
}

QWidget* SpinBoxDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if((index.column() == 1) & (index.row() == 1))
    {
        QDoubleSpinBox* editor = new QDoubleSpinBox(parent);

        editor->setFrame(false);
        editor->setMinimum(0);
        editor->setMaximum(1);
        editor->setDecimals(6);

        return editor;
    }

    return QStyledItemDelegate::createEditor(parent, option, index);
}

void SpinBoxDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    if((index.column() == 1) & (index.row() == 1))
    {
        double value = index.model()->data(index, Qt::EditRole).toDouble();

        QDoubleSpinBox* spinBox = static_cast<QDoubleSpinBox*>(editor);
        spinBox->setValue(value);
    }

    QStyledItemDelegate::setEditorData(editor, index);
}

void SpinBoxDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    if((index.column() == 1) & (index.row() == 1))
    {
        QDoubleSpinBox* spinBox = static_cast<QDoubleSpinBox*>(editor);
        spinBox->interpretText();
        double value = spinBox->value();

        model->setData(index, value, Qt::EditRole);
    }

    QStyledItemDelegate::setModelData(editor, model, index);
}

void SpinBoxDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if((index.column() == 1) & (index.row() == 1))
    {
        editor->setGeometry(option.rect);
    }

    QStyledItemDelegate::updateEditorGeometry(editor, option, index);
}

void SpinBoxDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyledItemDelegate::paint(painter, option, index);
}
